package soap.cibertec.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import soap.cibertec.entidad.ProductBean;
import soap.cibertec.interfaces.WsInterface;
import soap.cibertec.utils.MysqlDBConexion;


public class WsInterfaceImpl implements WsInterface{
	@Override
	public int saveProduct(ProductBean bean){
		
		int estado=-1;
		Connection cn=null;
		CallableStatement cstm=null;
		
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="call CREAR_PRODUCTO(?,?,?,?)";
			cstm=cn.prepareCall(sql);
			cstm.setString(1,bean.getNombre());
			cstm.setString(2,bean.getMarca());
			cstm.setDouble(3,bean.getPrecio());
			cstm.setInt(4,bean.getStock());
			estado=cstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(cstm!=null) cstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return estado;
	}

	@Override
	public List<ProductBean> listProductsxPrice(double priceOne, double priceTwo) {
		
		List<ProductBean> lista=new ArrayList<ProductBean>();
		ProductBean bean=null;
		Connection cn=null;
		CallableStatement cstm=null;
		ResultSet rs=null;
		
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="call LISTADO_POR_RANGO(?,?)";
			cstm=cn.prepareCall(sql);
			
			if(priceOne>priceTwo) {
				double temp = priceOne;
				priceOne = priceTwo;
				priceTwo = temp;
			}
			
			cstm.setDouble(1, priceOne);
			cstm.setDouble(2, priceTwo);
			
			rs=cstm.executeQuery();
			while(rs.next()) {
				bean=new ProductBean();
				bean.setIdproducto(rs.getInt(1));
				bean.setNombre(rs.getString(2));
				bean.setMarca(rs.getString(3));
				bean.setPrecio(rs.getDouble(4));
				bean.setStock(rs.getInt(5));
				lista.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(rs!=null) rs.close();
				if(cstm!=null) cstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return lista;
	}
	
	
		
}


