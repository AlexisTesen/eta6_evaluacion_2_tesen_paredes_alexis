package soap.cibertec.entidad;

public class ProductBean {

	private int idproducto;
	private String nombre;
	private String marca;
	private Double precio;
	private int stock;

	public ProductBean() {

	}

	public ProductBean(int idproducto, String nombre, String marca, Double precio, int stock) {
		this.idproducto = idproducto;
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
		this.stock = stock;
	}

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idProducto) {
		this.idproducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

}
