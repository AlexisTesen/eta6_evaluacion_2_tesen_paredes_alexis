package soap.cibertec.interfaces;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import soap.cibertec.entidad.ProductBean;

@WebService(name = "servicio")
public interface WsInterface {

	@WebMethod
	public int saveProduct(@WebParam(name = "producto") ProductBean bean);

	@WebMethod
	public List<ProductBean> listProductsxPrice(
			@WebParam(name = "priceOne") double priceOne,
			@WebParam(name = "priceTwo") double priceTwo
			);

}
