package soap.cibertec.servicio;

import java.util.List;

import javax.jws.WebService;

import soap.cibertec.dao.WsInterfaceImpl;
import soap.cibertec.entidad.ProductBean;
import soap.cibertec.interfaces.WsInterface;

@WebService(name = "servicioProducto", endpointInterface = "soap.cibertec.interfaces.WsInterface")
public class WsImplementado implements WsInterface {

	private WsInterfaceImpl dao = new WsInterfaceImpl();

	@Override
	public int saveProduct(ProductBean bean) {
		return dao.saveProduct(bean);
	}

	@Override
	public List<ProductBean> listProductsxPrice(double priceOne, double priceTwo) {
		return dao.listProductsxPrice(priceOne, priceTwo);
	}

}
