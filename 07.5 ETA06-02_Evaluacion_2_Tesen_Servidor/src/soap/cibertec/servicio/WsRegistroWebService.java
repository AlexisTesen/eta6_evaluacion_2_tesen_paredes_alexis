package soap.cibertec.servicio;

import javax.xml.ws.Endpoint;

public class WsRegistroWebService {

	public static void main(String[] args) {
		
		WsImplementado objeto = new WsImplementado(); 
		String URL = "http://localhost:8050/servicioProductos?wsdl";
		Endpoint edp = Endpoint.publish(URL, objeto);
		System.out.println(edp.isPublished());
		
	}

}

