<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html lang="esS">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/bootstrapValidator.min.css" rel="stylesheet">
<link href="css/bootstrap-datepicker.css" rel="stylesheet">
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrapValidator.min.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="sweetalert/sweetalert.min.js"></script>




<title>Productos</title>
<style>
.modal-header, h4, .close {
	background-color: #286090;
	color: white !important;
	text-align: center;
	font-size: 20px;
}

.btn-warning, .btn-info {
	width: 100px;
}

.btn-danger, .btn-primary {
	width: 160px;
}
</style>
</head>
<body>
	<div class="container">
	
		<h3 align="center">Consulta Productos por Precios</h3>
		
		<form class="form-inline" id="idConsulta">

			<div class="form-group">
				<label for="staticEmail">Rango de Precios (No importa el orden, mayor o menor primero el sistema lo Ordena)</label> 
			</div>
			
			<div class="form-group">
				<input class="form-control" id="idPriceOne" name="priceOne" placeholder="Ingrese el primer precio" />
				<input class="form-control" id="idPriceTwo" name="priceTwo" placeholder="Ingrese el segundo precio" />
			</div>
			
			<div class="form-group">
				<button type="button" class="btn btn-primary" id="validatetBtn">Consulta</button>
			</div>
			
		</form>
		
		<br>
		<p>
		<div id="divEmpleado">
			<table id="id_table" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Codigo</th>
						<th>Nombre</th>
						<th>Marca</th>
						<th>Precio</th>
						<th>Stock</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>

		</div>
	</div>


	<script type="text/javascript">
		$(".btn-primary").click(
				function() {
					var priceOneS;
					var priceTwoS;
					priceOneS = $("#idPriceOne").val();
					priceTwoS = $("#idPriceTwo").val();
					$("#id_table tbody").empty();
					$.getJSON("listProductsxPrice", {
						priceOne : priceOneS,
						priceTwo : priceTwoS
					}, function(response) {
						$.each(response.listaProductos, function(index, item) {
							$("#id_table").append(
									"<tr><td>" + item.idproducto + "</td><td>"
											+ item.nombre + "</td><td>"
											+ item.marca + "</td><td>"
											+ item.precio + "</td><td>"
											+ item.stock + "</td></tr>");
						})
					})

				});
		
		$(document)
		.ready(
				function() {
					$('#idConsulta')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											priceOne : {

												selector : '#idPriceOne',
												validators : {
													notEmpty : {
														message : 'El campo primer Precio es requerido. No puede estar vacio'
													},
													numeric : {
														message : 'Ingrese un n�mero v�lido'
													}
												}
											},
											
											priceTwo : {
												selector : '#idPriceTwo',
												validators : {
													notEmpty : {
														message : 'El campo segundo Precio es requerido. No puede estar vacio'
													},
													numeric : {
														message : 'Ingrese un n�mero v�lido'
													}
												}
											}

										}
									});
				});
				
				
				
	</script>

</body>
</html>












