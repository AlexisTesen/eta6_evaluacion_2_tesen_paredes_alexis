package soap.cibertec.servicio;

import java.util.List;

import soap.cibertec.interfaces.ProductBean;

public class ProductService {

	private WsImplementadoService ws = null;

	public List<ProductBean> listProductsxPrice(double priceOne, double priceTwo) {

		List<ProductBean> lista = null;
		try {
			ws = new WsImplementadoService();
			Servicio soap = ws.getServicioProductoPort();
			lista = soap.listProductsxPrice(priceOne, priceTwo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	public int saveProduct(ProductBean bean) {
		int salida = -1;
		try {
			ws = new WsImplementadoService();
			Servicio soap = ws.getServicioProductoPort();
			salida = soap.saveProduct(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salida;
	}

}
