
package soap.cibertec.servicio;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import soap.cibertec.interfaces.ObjectFactory;
import soap.cibertec.interfaces.ProductBean;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "servicio", targetNamespace = "http://interfaces.cibertec.soap/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Servicio {


    /**
     * 
     * @param priceOne
     * @param priceTwo
     * @return
     *     returns java.util.List<soap.cibertec.interfaces.ProductBean>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "listProductsxPrice", targetNamespace = "http://interfaces.cibertec.soap/", className = "soap.cibertec.interfaces.ListProductsxPrice")
    @ResponseWrapper(localName = "listProductsxPriceResponse", targetNamespace = "http://interfaces.cibertec.soap/", className = "soap.cibertec.interfaces.ListProductsxPriceResponse")
    @Action(input = "http://interfaces.cibertec.soap/servicio/listProductsxPriceRequest", output = "http://interfaces.cibertec.soap/servicio/listProductsxPriceResponse")
    public List<ProductBean> listProductsxPrice(
        @WebParam(name = "priceOne", targetNamespace = "")
        double priceOne,
        @WebParam(name = "priceTwo", targetNamespace = "")
        double priceTwo);

    /**
     * 
     * @param producto
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "saveProduct", targetNamespace = "http://interfaces.cibertec.soap/", className = "soap.cibertec.interfaces.SaveProduct")
    @ResponseWrapper(localName = "saveProductResponse", targetNamespace = "http://interfaces.cibertec.soap/", className = "soap.cibertec.interfaces.SaveProductResponse")
    @Action(input = "http://interfaces.cibertec.soap/servicio/saveProductRequest", output = "http://interfaces.cibertec.soap/servicio/saveProductResponse")
    public int saveProduct(
        @WebParam(name = "producto", targetNamespace = "")
        ProductBean producto);

}
