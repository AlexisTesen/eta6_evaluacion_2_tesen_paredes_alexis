package soap.cliente.action;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import soap.cibertec.interfaces.ProductBean;
import soap.cibertec.servicio.ProductService;
@ParentPackage("dawi")
public class ProductAction extends ActionSupport{
	
	
	public List<ProductBean> listaProductos;
	public ProductBean producto;
	private double priceOne;
	private double priceTwo;
	
	private Map<String,Object> session=ActionContext.getContext().getSession();
	
	private ProductService servicio;
	
	public ProductAction() {
		servicio=new ProductService();
	}
	
	
	@Action(value="/listProductsxPrice",results= {@Result(name="ok",type="json")})
	public String listAllLibroAtCantidad() {
		listaProductos=servicio.listProductsxPrice(priceOne, priceTwo);
		return "ok";
	}
	
	
	
	@Action(value="/saveProduct",results= {@Result(name="ok",location="/producto.jsp")})
	public String saveProduct() {
		int salida;
		salida=servicio.saveProduct(producto);
		if(salida!=-1)
			session.put("MENSAJE","Registro exitoso");
		else
			session.put("MENSAJE","Error al Registrar");
		return "ok";
	}

	public List<ProductBean> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductBean> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public ProductBean getProducto() {
		return producto;
	}

	public void setProducto(ProductBean producto) {
		this.producto = producto;
	}

	public double getPriceOne() {
		return priceOne;
	}

	public void setPriceOne(double priceOne) {
		this.priceOne = priceOne;
	}

	public double getPriceTwo() {
		return priceTwo;
	}

	public void setPriceTwo(double priceTwo) {
		this.priceTwo = priceTwo;
	}  
	
}






