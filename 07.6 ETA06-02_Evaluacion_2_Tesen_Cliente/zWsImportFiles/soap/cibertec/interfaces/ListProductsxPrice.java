
package soap.cibertec.interfaces;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para listProductsxPrice complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="listProductsxPrice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="priceOne" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="priceTwo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listProductsxPrice", propOrder = {
    "priceOne",
    "priceTwo"
})
public class ListProductsxPrice {

    protected double priceOne;
    protected double priceTwo;

    /**
     * Obtiene el valor de la propiedad priceOne.
     * 
     */
    public double getPriceOne() {
        return priceOne;
    }

    /**
     * Define el valor de la propiedad priceOne.
     * 
     */
    public void setPriceOne(double value) {
        this.priceOne = value;
    }

    /**
     * Obtiene el valor de la propiedad priceTwo.
     * 
     */
    public double getPriceTwo() {
        return priceTwo;
    }

    /**
     * Define el valor de la propiedad priceTwo.
     * 
     */
    public void setPriceTwo(double value) {
        this.priceTwo = value;
    }

}
