
package soap.cibertec.interfaces;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the soap.cibertec.interfaces package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListProductsxPriceResponse_QNAME = new QName("http://interfaces.cibertec.soap/", "listProductsxPriceResponse");
    private final static QName _SaveProduct_QNAME = new QName("http://interfaces.cibertec.soap/", "saveProduct");
    private final static QName _ListProductsxPrice_QNAME = new QName("http://interfaces.cibertec.soap/", "listProductsxPrice");
    private final static QName _SaveProductResponse_QNAME = new QName("http://interfaces.cibertec.soap/", "saveProductResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: soap.cibertec.interfaces
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveProductResponse }
     * 
     */
    public SaveProductResponse createSaveProductResponse() {
        return new SaveProductResponse();
    }

    /**
     * Create an instance of {@link ListProductsxPriceResponse }
     * 
     */
    public ListProductsxPriceResponse createListProductsxPriceResponse() {
        return new ListProductsxPriceResponse();
    }

    /**
     * Create an instance of {@link SaveProduct }
     * 
     */
    public SaveProduct createSaveProduct() {
        return new SaveProduct();
    }

    /**
     * Create an instance of {@link ListProductsxPrice }
     * 
     */
    public ListProductsxPrice createListProductsxPrice() {
        return new ListProductsxPrice();
    }

    /**
     * Create an instance of {@link ProductBean }
     * 
     */
    public ProductBean createProductBean() {
        return new ProductBean();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListProductsxPriceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.cibertec.soap/", name = "listProductsxPriceResponse")
    public JAXBElement<ListProductsxPriceResponse> createListProductsxPriceResponse(ListProductsxPriceResponse value) {
        return new JAXBElement<ListProductsxPriceResponse>(_ListProductsxPriceResponse_QNAME, ListProductsxPriceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.cibertec.soap/", name = "saveProduct")
    public JAXBElement<SaveProduct> createSaveProduct(SaveProduct value) {
        return new JAXBElement<SaveProduct>(_SaveProduct_QNAME, SaveProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListProductsxPrice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.cibertec.soap/", name = "listProductsxPrice")
    public JAXBElement<ListProductsxPrice> createListProductsxPrice(ListProductsxPrice value) {
        return new JAXBElement<ListProductsxPrice>(_ListProductsxPrice_QNAME, ListProductsxPrice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.cibertec.soap/", name = "saveProductResponse")
    public JAXBElement<SaveProductResponse> createSaveProductResponse(SaveProductResponse value) {
        return new JAXBElement<SaveProductResponse>(_SaveProductResponse_QNAME, SaveProductResponse.class, null, value);
    }

}
