-- Para el Examen 2 de Desarrollo de Servicio Web 2
drop database CL2_Services;
create database CL2_Services;
use CL2_Services;

create table tb_producto(
idProducto int primary key auto_increment,
nombre varchar(30) not null,
marca varchar(30) not null,
precio double not null,
stock int not null
);

insert into tb_producto values 
(default,'Producto 1','Marca 1',25.5,15),
(default,'Producto 2','Marca 2',14.2,5),
(default,'Producto 3','Marca 3',7.8,50),
(default,'Producto 4','Marca 4',36,30),
(default,'Producto 5','Marca 5',9.3,8),
(default,'Producto 6','Marca 6',11.25,2);

    DELIMITER //
    CREATE PROCEDURE LISTADO_POR_RANGO (IN precio_uno double, IN precio_dos double)
    BEGIN
    SELECT * FROM TB_PRODUCTO WHERE PRECIO >= precio_uno and PRECIO <=precio_dos;
    END;
    
	DELIMITER //
    CREATE PROCEDURE CREAR_PRODUCTO (
    IN nombre varchar(30),
    IN marca varchar(30),
    IN precio double,
    IN stock int)
    BEGIN
    insert into tb_producto values 
	(default,nombre,marca,precio,stock);
    END;
    
    CALL LISTADO_POR_RANGO(1,11.26);
    CALL CREAR_PRODUCTO('Producto 7','Marca 7',1.25,90);
    
    select * from tb_producto;
